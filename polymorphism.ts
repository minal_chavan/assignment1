
class Animals{
    name:string
    colour:string
    constructor(animalname,animalcolour){
    this.name=animalname
    this.colour=animalcolour
    }
    property(dist:number){
    console.log("%s move %d meters",this.name,dist)
    console.log("%s has colour %s",this.name,this.colour)
    }
}

//var a1=new Animals('tomy')
//a1.move(10)

class Aquatic extends Animals {
    constructor(name: string,colour:string) { super(name,colour); }
    move(distanceInMeters = 5) {
        console.log("%s swims..",this.name);
        super.property(distanceInMeters);
    }
    
}

class Terrestrial extends Animals {
    constructor(name: string,colour:string) { super(name,colour); }
    move(distanceInMeters = 45) {
        console.log("%s walks..",this.name);
        super.property(distanceInMeters);
    }
    
}

class Amphibians extends Animals {
    constructor(name: string,colour:string) { super(name,colour); }
    move    (distanceInMeters = 45) {
        console.log("Galloping...");
        //super.move(distanceInMeters);
        super.property(distanceInMeters);
    }
}

var fish = new Aquatic("Fish_Dolphin",'black');
var rabbit = new Terrestrial("Rabbit",'white');
fish.move(55)
rabbit.move(33)