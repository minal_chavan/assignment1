var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Animals = /** @class */ (function () {
    function Animals(animalname, animalcolour) {
        this.name = animalname;
        this.colour = animalcolour;
    }
    Animals.prototype.property = function (dist) {
        console.log("%s move %d meters", this.name, dist);
        console.log("%s has colour %s", this.name, this.colour);
    };
    return Animals;
}());
//var a1=new Animals('tomy')
//a1.move(10)
var Aquatic = /** @class */ (function (_super) {
    __extends(Aquatic, _super);
    function Aquatic(name, colour) {
        return _super.call(this, name, colour) || this;
    }
    Aquatic.prototype.swim = function (distanceInMeters) {
        if (distanceInMeters === void 0) { distanceInMeters = 5; }
        console.log("%s swims..", this.name);
        _super.prototype.property.call(this, distanceInMeters);
    };
    return Aquatic;
}(Animals));
var Terrestrial = /** @class */ (function (_super) {
    __extends(Terrestrial, _super);
    function Terrestrial(name, colour) {
        return _super.call(this, name, colour) || this;
    }
    Terrestrial.prototype.walk = function (distanceInMeters) {
        if (distanceInMeters === void 0) { distanceInMeters = 45; }
        console.log("%s walks..", this.name);
        _super.prototype.property.call(this, distanceInMeters);
    };
    return Terrestrial;
}(Animals));
var Amphibians = /** @class */ (function (_super) {
    __extends(Amphibians, _super);
    function Amphibians(name, colour) {
        return _super.call(this, name, colour) || this;
    }
    Amphibians.prototype.swim_walk = function (distanceInMeters) {
        if (distanceInMeters === void 0) { distanceInMeters = 45; }
        console.log("Galloping...");
        //super.move(distanceInMeters);
        _super.prototype.property.call(this, distanceInMeters);
    };
    return Amphibians;
}(Animals));
var fish = new Aquatic("Fish_Dolphin", 'black');
var rabbit = new Terrestrial("Rabbit", 'white');
fish.swim();
rabbit.walk(34);
