
abstract class Animal {
	name:string
	constructor(name:string) {

	}
	move(n:number):void{
		console.log("%s move %d meters",this.name,n)
	}
	abstract colour(colour:string):void;
}

class Aquatic extends Animal {

	name:string 

	constructor(name1:string) {
		super("animal")
		this.name = name1
		//this.colour = colour1
	}

	move1(n=45){
		 console.log("%s swims..",this.name);
         super.move(n)
	}
	colour(colour1):void{
		console.log("%s has %s colour",this.name,colour1)

	}
}


class Terestial extends Animal {

	name:string 

	constructor(name1:string) {
		super("animal	")
		this.name = name1
		//this.colour = colour1
	}

	move1(n=45){
		 console.log("%s walks..",this.name);
         super.move(n)
	}
	colour(colour1):void{
		console.log("%s has %s colour",this.name,colour1)

	}
}


var fish=new Aquatic('Fish')
var dog=new Terestial('Dog')
fish.move1(333)
fish.colour("black")
dog.move1(122)
dog.colour("black")

